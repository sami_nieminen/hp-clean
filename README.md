# HP Clean

This program was made to speed up the installation of HP Elite/Pro series of laptops / desktops.
Support for other OEM's is planned, but for now, some parts of the cleanup only work on HP laptops / desktops.

**Remember** this is meant to be used just as a tool for pre-cleaning computers, before starting the actual preinstall/customization process.

# Features
## Removal of HP preinstalled "unnecessary" programs
Keyboard shortcuts, software that nags the users, anything that pop-ups or suggests something. You most likely won't ever need these.
## Removal of Windows apps for all users
This script will remove **all** Windows "apps", excluding Office, Sticky Notes and Store.
## Installs HP SDM (SoftPaq Download Manager)
The most usefull tool HP has released for Elite/Pro series computers. Allows you to install all the needed driver/firmware updates in one tool. It is not installed, or sometimes it is an old version on HP preinstalled laptops.

# How to use

1. Download this repository by clicking download
2. Unzip the files to the computer
3. Doubleclick `runthis.bat`
4. Click **Yes** on the UAC prompt
5. Select the tasks you want to run


# TODO

- Add support for other OEM's
- Add support for installing software packages with Chocolatey
- Add support for enabling & changing local admin password
- Add support for renaming PC
- Add support for automatic script recall after reboot



Copyright
Sami Nieminen 2019
Licenced under GNU-GPLv3