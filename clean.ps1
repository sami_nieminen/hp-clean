﻿# File check funtions
Function Not-Exist { -not (Test-Path $args) }
Set-Alias !Exist Not-Exist -Option "Constant, AllScope"
Set-Alias Exist Test-Path -Option "Constant, AllScope"

$ForeGroundColor = "Green"
Function Load-MainMenu {  
[cmdletbinding()]
param()
    ##########################################
    #                Main Menu               #
    ##########################################
    Write-Host `n"Cleaner v 0.1 v" $ncVer -ForeGroundColor $ForeGroundColor
    Write-Host `n"Type 'q' or hit enter to drop to quit"`n
    Write-Host -NoNewLine "<" -ForegroundColor $ForeGroundColor
    Write-Host -NoNewLine "Cleanup"
    Write-Host -NoNewLine ">" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "["
    Write-Host -NoNewLine "R" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "]"

    Write-Host -NoNewLine `t`n "R1 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Remove HP extra software"`n`n


    Write-Host -NoNewLine "<" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "Install"
    Write-Host -NoNewLine ">" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "["
    Write-Host -NoNewLine "I" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "]"

    Write-Host -NoNewLine `t`n "I1 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Download and install HP Image Assistant"
    Write-Host -NoNewLine `t`n "I2 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Download and install other software"`n`n

    Write-Host -NoNewLine "<" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "All"
    Write-Host -NoNewLine ">" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "["
    Write-Host -NoNewLine "A" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "]"

    Write-Host -NoNewLine `t`n "A1 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Run all steps"
    Write-Host -NoNewLine `t`n "A2 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Run all steps and install Teamviewer, Chrome and Firefox"`n`n

    $sel = Read-Host "Which option?"

    Switch ($sel) {
        "R1" {x64-Program-Uninstaller;Load-MainMenu}
        
        "I1" {HPIA-Install;Load-MainMenu}
        "I2" {Load-InstallerMenu}
        
        "A1" {x64-Program-Uninstaller;HPIA-Install;Load-MainMenu}
        "A2" {x64-Program-Uninstaller;HPIA-Install;Firefox-Install;Chrome-Install;TeamViewer-Install;Load-MainMenu}

        {($_ -like "*q*") -or ($_ -eq "")} {
            Write-Host `n"No input or 'q' seen... dropping to shell" -ForeGroundColor $ForeGroundColor
            Write-Host "Made by Sami"`n
            Sleep 5
            exit
        }
    }
    ##########################################
    #            End Main Menu               #
    ##########################################
}

$ForeGroundColor = "Green"
Function Load-InstallerMenu {  
[cmdletbinding()]
param()
    ##########################################
    #             Installer Menu             #
    ##########################################
    Write-Host `n"Cleaner v 0.1 v" $ncVer -ForeGroundColor $ForeGroundColor
    Write-Host `n"Type 'q' or hit enter to drop to quit"`n
    Write-Host -NoNewLine "<" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "Choose your software"
    Write-Host -NoNewLine ">" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "["
    Write-Host -NoNewLine "S" -ForeGroundColor $ForeGroundColor
    Write-Host -NoNewLine "]"

    Write-Host -NoNewLine `t`n "S1 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Firefox"
    Write-Host -NoNewLine `t`n "S2 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Chrome"
    Write-Host -NoNewLine `t`n "S3 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "TeamViewerQS to Public desktop"`n`n
    Write-Host -NoNewLine `t`n "S4 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Run all steps"`n`n
    Write-Host -NoNewLine `t`n "S5 - " -ForeGroundColor $ForeGroundColor
    Write-host -NoNewLine "Back to main menu"`n`n

    $sel = Read-Host "Which option?"

    Switch ($sel) {
        "S1" {Firefox-Install;Load-InstallerMenu}
        "S2" {Chrome-Install;Load-InstallerMenu}
        "S3" {TeamViewer-Install;Load-InstallerMenu}
        "S4" {Firefox-Install;Chrome-Install;TeamViewer-Install;Load-InstallerMenu}
        "S5" {Load-MainMenu}

        {($_ -like "*q*") -or ($_ -eq "")} {
            Write-Host `n"No input or 'q' seen... dropping to main menu" -ForeGroundColor $ForeGroundColor
            Sleep 5
            Load-MainMenu
        }
    }
    ##########################################
    #         End Installer Menu             #
    ##########################################
}

# Main loop for removing HP software
Function x64-Program-Uninstaller {
    # Variable for GetWMIC
    $Programs = gwmi Win32_Product

    #Variables for removing HP software
    $AppName = 'HP Collaboration Keyboard Runtime',
    'HP Collaboration Keyboard',
    'HP Collaboration Keyboard For Cisco Ucc',
    'HP Connection Optimizer',
    'HP Device Access Manager',
    'HP Documentation',
    'HP System Default Settings',
    'HP JumpStart Apps',
    'HP JumpStart Launch',
    'HP JumpStart Bridge',
    'HP MAC Address Manager',
    'HP Notifications',
    'HP Support Assistant',
    'HP Sure Recover',
    'HP Sure Run',
    'HP Sure Click',
    'HP Velocity',
    'HPWorkWise64',
    'HP PhoneWise Drivers',
    'HP Client Security Manager',
    'HP ePrint SW',
    'HP Registration Service',
    'HP Recovery Manager',
    'HP Wolf Security',
    'HP Security Update Service'

    Foreach($Program in $AppName) {
        $Programs | Where-Object {$_.Name -eq "$Program" } | ForEach-Object -Process {$_.Uninstall()}
    }

    # Variable for using MSI to uninstall HP software
    $x64App = Get-ChildItem HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ | ? { $_ -match "HP *" }

    # Main loop for MSI uninstall
    If ($x64App) {
        $UninstallPath = ($x64App |Get-ItemProperty).UninstallString
        Start-Process -NoNewWindow -FilePath $UninstallPath -ArgumentList " /s"
    }

    # Uninstall HP Documentation
    $HPDocumentation = "C:\Program Files\HP\Documentation\Doc_uninstall.cmd"
    If (Exist $HPDocumentation) {
        Write-Host "Uninstalling HP Documentation."
        Start-Process -NoNewWindow $HPDocumentation
    } Else {
        Write-Host "HP Documentation not found, continuing."
    }

    # Uninstall HP Connection Optimizer
    $HPConnectionOptimizer = "C:\Program Files (x86)\InstallShield Installation Information\{6468C4A5-E47E-405F-B675-A70A70983EA6}\setup.exe"
    If (Exist $HPConnectionOptimizer) {
        Write-Host "Uninstalling HP Connection Optimizer."
        Start-Process -NoNewWindow $HPConnectionOptimizer -ArgumentList " -runfromtemp -l0x041d  -removeonly -silent"
    } Else {
        Write-Host "Connection Optimizer not found, continuing."
    }

    # Uninstall HP Collaboration Keyboard
    $HPCollaborationKeyboard = "C:\Program Files (x86)\InstallShield Installation Information\{3FF0ED81-EE68-4075-BB62-945D6C785CE1}\Setup.exe"
    If (Exist $HPCollaborationKeyboard) {
        Write-Host "Uninstalling HP Collaboration Keyboard."
        Start-Process -NoNewWindow $HPCollaborationKeyboard -ArgumentList " -remove -runfromtemp -silent"
    } Else {
        Write-Host "Collaboration Keyboard not found, continuing."
    }
    
    # Uninstall HP SureSense
    $HPSureSense = "C:\Program Files\HP Sure Sense\HPSureSense.msi"
    If (Exist $HPSureSense) {
        Write-Host "Uninstalling HP SureSense."
        msiexec.exe /x "$HPSureSense" /q
    } Else {
        Write-Host "HP Sure Sense not found, continuing."
    }
}

# Installs Firefox
Function Firefox-Install {
    $LocalTempDir = $env:TEMP
    $FirefoxInstaller = "FirefoxInstaller.exe"
    $FirefoxDownloadUrl = "https://download-installer.cdn.mozilla.net/pub/firefox/releases/89.0.2/win64/en-US/Firefox%20Setup%2089.0.2.exe"
    (New-Object System.Net.WebClient).DownloadFile($FirefoxDownloadUrl, "$LocalTempDir\$FirefoxInstaller")
    & "$LocalTempDir\$FirefoxInstaller" -ms
    Write-Host "Starting Mozilla Firefox install."
    $Process2Monitor =  "FirefoxInstaller"
    Do { 
        $ProcessesFound = Get-Process | ?{$Process2Monitor -contains $_.Name} | Select-Object -ExpandProperty Name
        If ($ProcessesFound) { 
            "Still running: $($ProcessesFound -join ', ')" | Write-Host
            Start-Sleep -Seconds 5
        } Else { 
            rm "$LocalTempDir\$FirefoxInstaller" -ErrorAction SilentlyContinue -Verbose 
        } 
    } Until (!$ProcessesFound)
}



# Installs version 375.126 of Google Chrome
Function Chrome-Install {
    $LocalTempDir = $env:TEMP
    $ChromeInstaller = "ChromeInstaller.exe"
    $ChromeDownloadUrl = "http://dl.google.com/chrome/install/375.126/chrome_installer.exe"
    (New-Object System.Net.WebClient).DownloadFile($ChromeDownloadUrl, "$LocalTempDir\$ChromeInstaller")
    & "$LocalTempDir\$ChromeInstaller" /silent /install
    Write-Host "Starting Google Chrome install"
    $Process2Monitor =  "ChromeInstaller"
    Do { 
        $ProcessesFound = Get-Process | ?{$Process2Monitor -contains $_.Name} | Select-Object -ExpandProperty Name
        If ($ProcessesFound) { 
            "Still running: $($ProcessesFound -join ', ')" | Write-Host
            Start-Sleep -Seconds 5
        } Else { 
            rm "$LocalTempDir\$ChromeInstaller" -ErrorAction SilentlyContinue -Verbose 
        } 
    } Until (!$ProcessesFound)
}


# Nasty hacks for downloading Custom Teamviewer.
Function TeamViewer-Install {
    $CustomTeamViewerUrl = "https://customdesign.teamviewer.com/download/version_15x/8fu2a77_windows/TeamViewerQS.exe"
    $CustomTeamViewerFile = "C:\Users\Public\Desktop\TeamViewerQS.exe"
    If (Not-Exist $CustomTeamViewerFile) {
        $LoginUrl = "https://get.teamviewer.com/inhelp";                 # First we define the url to get the cookie
        $ie = New-Object -com internetexplorer.application;              # Then we initialize browser
        $ie.visible = $false;                                            # Set to $true for debugging
        $ie.navigate($loginUrl);                                         # Navigate to URL
        while ($ie.Busy -eq $true) { Start-Sleep -Seconds 1; }           # Wait for browser idle to get the save prompt
        $ie.Quit()                                                       # Close the browser
        Write-Host "TeamViewer not found, downloading."
        Invoke-WebRequest -Uri $CustomTeamViewerUrl -OutFile $CustomTeamViewerFile
    } Else {
        Write-Host "TeamViewer found, not downloading."
    }
}

# Checks first if the MSI or EXE file is already there, and downloads/unpacks the MSI file for installer
Function HPIA-Install {
    $CurrentLocation=Get-Location
    If (Not-Exist $CurrentLocation\hp-hpia-5.1.1.exe) {
        Write-Host "HPIA EXE file not found, downloading."
        wget -Uri https://hpia.hpcloud.hp.com/downloads/hpia/hp-hpia-5.1.1.exe -OutFile hp-hpia-5.1.1.exe
    } Else {
        Write-Host "HPIA Found, installing silently."
    }

    # Install SDM and handle error codes.
    Write-Host "Starting HPIA install."
    $Result = (Start-Process -FilePath "hp-hpia-5.1.1.exe" -ArgumentList "/s /e /f C:\HPIA" -Wait -Passthru).ExitCode
    If ($($Result) -eq '1168') {
        Write-Host "HPIA installed successfully!"
    } Else {
        Write-Warning -Message "HPIA failed to install with msi exit code: $($Result)"
    }
}

Load-MainMenu